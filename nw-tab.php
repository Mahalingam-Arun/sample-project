<!DOCTYPE html>
<html>
<head>
	<title>New tab</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="./css/nw-tab.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</script>
<style type="text/css">
  #nav-home
{
  width: 100%;
  height: 609px;
    background: linear-gradient(225deg, #7BDF5F ,#787FF6,#4ADEDE,#1CA7EC,#1F2F98);
    background-size: 800% 800%;
    animation: AnimationName 16s ease infinite;
    
}

@keyframes AnimationName {
    0%{background-position:0% 80%}
    50%{background-position:100% 80%}
    100%{background-position:0% 80%}
}
#first-container-fluid-1-p
{
  color: black;
  font-weight: bold;
  font-size: 1.5rem;
  font-family:futura;

}
#input-1{
    background-color:#fff420;
     border: 4px solid white;
     border-radius: 1px;
     color: white;
     font-weight: bold;
     padding: 10px;
  box-shadow: 5px 3px 2px #fff5f4;
  font-family: oswald;
   margin-top: 260px;
   margin-left: 395px;
}
#input-1:hover
{
    background:#fff420;
     border: 4px solid white;
     border-radius: 1px;
     color: white;
     padding: 10px;
  box-shadow: 5px 3px 2px #909497;
   font-family: oswald;
    margin-top: 260px;
   margin-left: 395px;

}
#input[type=file]{
    background:red;

}
#input-2{
    background: #ffd900;
     border: 4px solid white;
     border-radius: 1px;
     color: white;
     font-weight: bold;
     padding: 10px;
  box-shadow: 5px 3px 2px #fff5f4;
  font-family: futura;
   margin-left: 40px;
  

}
#input-2:hover
{
    background: #ffd900;
     border: 4px solid white;
     border-radius: 1px;
     color: white;
     padding: 15px;
  box-shadow: 5px 3px 2px #909497;
   font-family: futura;
    

}
#nav-profile-h3
{
  font-family: verdana;
  font-weight: lighter;
  
}
#nav-profile-img
{
  width: 300px;
  height: 300px;
  margin-left: 50px;
  margin-top: 30px;
  border: 8px solid yellow;
}
#nav-profile-img:hover
{
  width: 300px;
  height: 300px;
  margin-left: 50px;
  margin-top: 30px;
  border: 4px solid yellow;
}
#nav-logout-tab
{
  border:1px solid #3b5998;
  color: white;
  background-color:#3b5998;
  font-weight: bold;
}
#nav-logout-tab:hover
{
  border:1px solid #3b5998;
  color: #808000;
  background-color:  #3b5998;
  font-weight: bold;
}
#logout-1
{
  padding: 5px;
  background-color: black;
  color: white;
  text-decoration: none;
  font-weight: bold;
  font-size: 1rem;
}
</style>
</head>
<body>
  <?php
require('db.php');
    
session_start();
 $uids = $_SESSION['uid'];
    if(isset($_REQUEST['upload'])){
 
 if(isset($_REQUEST['upload'])){

         if(isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0){
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
        $filename = $_FILES["photo"]["name"];
        $filetype = $_FILES["photo"]["type"];
        $filesize = $_FILES["photo"]["size"];
    
        // Verify file extension
       $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format.");
    
        // Verify file size - 5MB maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");
    
        // Verify MYME type of the file
        if(in_array($filetype, $allowed)){
            // Check whether file exists before uploading it
            if(file_exists("images/" . $filename)){
                echo $filename . " is already exists.";
            } else{
               echo $path = "images/" . $filename;
                 move_uploaded_file($_FILES["photo"]["tmp_name"], "images/" . $filename);
                echo "";
                   $query = "INSERT into `image` (userid,image)
VALUES ('$uids','$path')";


                  $insert = mysqli_query($con,$query);
            } 

        } else{
            echo "Error: There was a problem uploading your file. Please try again."; 
        }
    } else{
        echo "Error: " . $_FILES["photo"]["error"];
    }


      }
      }

?>
<nav class="fixed-top" id="nav-container">
  <div class="nav nav-tabs w-100 nav-fill" id="nav-tab" role="tablist">
    <a class="nav-item nav-link " id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
    	<img src="./images/upload.PNG">
    </a>
    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false" >
    	<img src="./images/profile.PNG">
    </a>
   <a href="logout.php" id="logout-1">logout</a>
  </div>

</nav>
<div class="tab-content " id="nav-tabContent">
  <div class="tab-pane fade show active container-fluid " id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab" >
  			
  			
            <p class="text-center" id="first-container-fluid-1-p">
            	SHARE YOUR THOUGHTS
            </p>
<form  action = "" method = "POST" enctype = "multipart/form-data">
 
    
    <input type = "file" name = "photo" id="input-1">
         <input type = "submit" id="input-2" name="upload">


  
</form>

  </div>





  <div class="tab-pane fade " id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

  <h3 class="text-center" id="nav-profile-h3">Profile</h3>

<?php


  $fetch_img = mysqli_query($con,"SELECT * FROM  `image` WHERE userid='$uids'"); 
  while($fetch_all = mysqli_fetch_array($fetch_img)){
  $imgs = $fetch_all['image'];
 ?>



  
     <img src="<?php echo $imgs;?>" id="nav-profile-img">
<?php
}

?>
  

  </div>
  
</div>



</body>
</html>