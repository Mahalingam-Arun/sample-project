-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2020 at 04:01 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db`
--

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(20) NOT NULL,
  `userid` varchar(55) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `userid`, `image`) VALUES
(8, '1', 'images/IMG-20190525-WA0003.jpg'),
(9, '10', 'images/2.png'),
(10, '', 'images/8.png'),
(11, '', 'images/10.png'),
(12, '', 'images/2018-03-30-11-06-49-865 - Copy.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `trn_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `trn_date`) VALUES
(1, '', '', 'd41d8cd98f00b204e9800998ecf8427e', '2020-08-04 12:09:29'),
(2, 'fgfd', 'gfdgfd@dss.com', 'a94e3d4848c846b4202d8e152b425c6d', '2020-08-04 12:13:34'),
(3, 'fgfd', 'gfdgfd@dss.com', 'a94e3d4848c846b4202d8e152b425c6d', '2020-08-04 12:22:12'),
(4, '12345', '12345@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '2020-08-04 12:23:06'),
(5, 'arun', 'gokulin@piraiin.com', '25d55ad283aa400af464c76d713c07ad', '2020-08-04 12:33:45'),
(6, 'arunlatha', 'arunlatha@gmail.com', '208a4499356cf8f8919eba4af937c883', '2020-08-04 16:06:35'),
(7, 'asd', '123456@gmail.com', '25d55ad283aa400af464c76d713c07ad', '2020-08-04 16:31:21'),
(8, 'asd', '123456@gmail.com', '25d55ad283aa400af464c76d713c07ad', '2020-08-04 16:36:43'),
(9, 'kattam', 'selva@gmail.com', '25d55ad283aa400af464c76d713c07ad', '2020-08-05 06:58:16'),
(10, 'a', 'a@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-06 14:08:28'),
(11, 'b', 'b@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '2020-08-06 14:08:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
